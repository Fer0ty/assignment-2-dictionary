ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: clean

main: main.o dict.o lib.o
	$(LD) -o $@ $^
	
lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $^

main.o: main.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ main.asm
        
clean:
	rm *.o
